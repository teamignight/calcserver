package mock;

import com.ignight.core.CoreData;
import com.ignight.data.GroupData;
import com.ignight.data.UserActivity;
import com.ignight.data.UserDna;
import com.ignight.data.VenueActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MockCoreData extends CoreData {
    private Map<Long, GroupData> groups = new HashMap<Long, GroupData>();
    private Map<Long, VenueActivity> activeVenues = new HashMap<Long, VenueActivity>();
    private Map<Long, UserActivity> userActivityMap = new HashMap<Long, UserActivity>();

    @Override
    public void clear() {
        groups.clear();
        activeVenues.clear();
        userActivityMap.clear();
        super.clear();
    }

    @Override
    public List<Long> getAllGroupIds() {
        return new ArrayList<Long>(groups.keySet());
    }

    @Override
    public GroupData getGroup(Long groupId) {
        return groups.get(groupId);
    }

    @Override
    public Set<Long> getUserIds() {
        return super.getUserIds();
    }

    @Override
    public UserDna getUserDna(Long userId) {
        return userDnas.get(userId);
    }

    @Override
    public void setUserDna(Long userId, UserDna dna) {
        super.setUserDna(userId, dna);
    }

    @Override
    public Set<Long> getUserGroups(Long userId) {
        return super.getUserGroups(userId);
    }

    @Override
    public VenueActivity getVenueActivity(Long venueId) {
        return activeVenues.get(venueId);
    }

    public void setVenueActivity(VenueActivity activity) {
        activeVenues.put(activity.id, activity);
    }

    @Override
    public UserActivity getUserActivity(Long userId) {
        return userActivityMap.get(userId);
    }

    public void setUserActivity(UserActivity activity) {
        userActivityMap.put(activity.userId, activity);
    }
}
