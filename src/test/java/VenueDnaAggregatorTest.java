import com.ignight.calcs.VenueDnaAggregator;
import com.ignight.data.CalcVenueDna;
import com.ignight.data.UserActivity;
import com.ignight.data.UserDna;
import com.ignight.data.VenueActivity;
import junit.framework.TestCase;
import mock.MockCoreData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;

public class VenueDnaAggregatorTest extends TestCase {
    private static final Logger logger = LoggerFactory.getLogger(VenueDnaAggregatorTest.class);

    VenueDnaAggregator venueDnaAggregator;
    VenueActivity venueActivity;
    ConcurrentHashMap<Long, UserActivity> userActivityMap;
    MockCoreData coreData;

    public void setUp() {
        coreData = new MockCoreData();
        venueActivity = new VenueActivity(1l);
        userActivityMap = new ConcurrentHashMap<Long, UserActivity>();
        venueDnaAggregator = new VenueDnaAggregator(venueActivity, userActivityMap, coreData);
    }

    public void testSorting() {
        UserDna dna = new UserDna(1l).ageId(0l).musicId(3l).musicId(2l).atmosphereId(4l).atmosphereId(3l).spendingLimit(0l);
        upvoteVenue(dna);
        UserDna dna2 = new UserDna(1l).ageId(0l).musicId(2l).musicId(3l).atmosphereId(3l).atmosphereId(4l).spendingLimit(0l);
        upvoteVenue(dna2);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).dnaId, 3);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 3);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).dnaId, 4);
    }

    public void testSingleUserSimpleDnaUpvote() {
        UserDna dna = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);

        upvoteVenue(dna);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 1000);
        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 3000);
    }

    public void testSingleUserSimpleDnaDownvote() {
        UserDna dna = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        downvoteVenue(dna);
        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 1000);
        assertEquals(calcVenueDna.downvotedDna.ageId, 0);
        assertEquals(calcVenueDna.downvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).count, 3000);
    }

    public void testSingleUserNotSimpleDnaUpvote() {
        UserDna dna = new UserDna(1l).ageId(2l)
                                     .musicId(0l).musicId(1l)
                                     .atmosphereId(2l).atmosphereId(4l).atmosphereId(5l)
                                     .spendingLimit(3l);
        upvoteVenue(dna);
        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 1000);

        assertEquals(calcVenueDna.upvotedDna.ageId, 2);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).count, 1000);
    }

    public void testSingleUserNotSimpleDnaDownvote() {
        UserDna dna = new UserDna(1l).ageId(2l)
                .musicId(0l).musicId(1l)
                .atmosphereId(2l).atmosphereId(4l).atmosphereId(5l)
                .spendingLimit(3l);
        downvoteVenue(dna);
        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 1000);

        assertEquals(calcVenueDna.downvotedDna.ageId, 2);
        assertEquals(calcVenueDna.downvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).count, 1000);
    }

    public void testSingleUserDiscountedActivityUpvote() {
        UserDna dna = new UserDna(1l).ageId(2l)
                .musicId(0l).musicId(1l)
                .atmosphereId(2l).atmosphereId(4l).atmosphereId(5l)
                .spendingLimit(3l);
        upvoteVenue(dna);
        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 1000);

        assertEquals(calcVenueDna.upvotedDna.ageId, 2);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).count, 1000);

        addUpvoteToUserActivity(dna.userId, 2l);
        calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 500);

        assertEquals(calcVenueDna.upvotedDna.ageId, 2);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 1500);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).count, 1000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 1500);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).count, 1000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).count, 500);

        addUpvoteToUserActivity(dna.userId, 3l);
        calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 333);

        assertEquals(calcVenueDna.upvotedDna.ageId, 2);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 1000);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).count, 2000/3);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 1000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).count, 2000/3);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(2).count, 1000/3);
    }

    public void testSingleUserDiscountedActivityDownvote() {
        UserDna dna = new UserDna(1l).ageId(2l)
                .musicId(0l).musicId(1l)
                .atmosphereId(2l).atmosphereId(4l).atmosphereId(5l)
                .spendingLimit(3l);
        downvoteVenue(dna);
        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 1000);

        assertEquals(calcVenueDna.downvotedDna.ageId, 2);
        assertEquals(calcVenueDna.downvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).count, 2000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).count, 1000);

        addDownvoteToUserActivity(dna.userId, 2l);
        calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 500);

        assertEquals(calcVenueDna.downvotedDna.ageId, 2);
        assertEquals(calcVenueDna.downvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).count, 1500);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).count, 1000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).count, 1500);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).count, 1000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).count, 500);

        addDownvoteToUserActivity(dna.userId, 3l);
        calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 333);

        assertEquals(calcVenueDna.downvotedDna.ageId, 2);
        assertEquals(calcVenueDna.downvotedDna.spendingLimit, 3);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).count, 3000 / 3);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(1).count, 2000 / 3);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).dnaId, 2);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).count, 3000 / 3);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).dnaId, 4);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(1).count, 2000 / 3);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).dnaId, 5);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(2).count, 1000 / 3);
    }

    public void testMultipleUserSimpleDnaUpvote() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);

        UserDna dna2 = new UserDna(2l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna2);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 2000);
        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 6000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 6000);
    }

    public void testMultipleUserSimpleDnaUpvoteDifferentAtmosphere() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);

        UserDna dna2 = new UserDna(2l).ageId(0l).musicId(0l).atmosphereId(1l).spendingLimit(0l);
        upvoteVenue(dna2);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 2000);
        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 6000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(1).count, 3000);
    }

    public void testMultipleUserSimpleDnaUpvoteDifferentMusic() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);

        UserDna dna2 = new UserDna(2l).ageId(0l).musicId(1l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna2);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 2000);
        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).dnaId, 1);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(1).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 6000);
    }

    public void testMultipleUserSimpleDnaUpvoteWithDiscounting() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);

        UserDna dna2 = new UserDna(2l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna2);
        addUpvoteToUserActivity(dna2.userId, 2l);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 1500);
        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 4500);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 4500);
    }

    public void testMultipleUserMultipleDiscounting() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);
        addUpvoteToUserActivity(dna1.userId, 2l);

        UserDna dna2 = new UserDna(2l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna2);
        addUpvoteToUserActivity(dna2.userId, 2l);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 1000);
        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 3000);
    }

    public void testMultipleUserUpvoteDownvote() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);

        UserDna dna2 = new UserDna(2l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        downvoteVenue(dna2);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 1000);
        assertEquals(calcVenueDna.downvotedDna.ageId, 0);
        assertEquals(calcVenueDna.downvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.downvotedDna.atmospherePreferences.get(0).count, 3000);

        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 1000);
        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 3000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 3000);
    }

    public void testMultipleUserUpvoteAgeSpendingDifference() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);

        UserDna dna2 = new UserDna(2l).ageId(1l).musicId(0l).atmosphereId(0l).spendingLimit(3l);
        upvoteVenue(dna2);

        UserDna dna3 = new UserDna(3l).ageId(1l).musicId(0l).atmosphereId(0l).spendingLimit(3l);
        upvoteVenue(dna3);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 3000);

        assertEquals(calcVenueDna.upvotedDna.ageId, 1);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 3);

        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 9000);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 9000);
    }

    public void testMultipleUserUpvoteAgeSpendingDifferenceWithDiscounting() {
        UserDna dna1 = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        upvoteVenue(dna1);

        UserDna dna2 = new UserDna(2l).ageId(1l).musicId(0l).atmosphereId(0l).spendingLimit(3l);
        upvoteVenue(dna2);
        addUpvoteToUserActivity(dna2.userId, 2l);
        addUpvoteToUserActivity(dna2.userId, 3l);

        UserDna dna3 = new UserDna(3l).ageId(1l).musicId(0l).atmosphereId(0l).spendingLimit(3l);
        upvoteVenue(dna3);
        addUpvoteToUserActivity(dna3.userId, 2l);

        CalcVenueDna calcVenueDna = venueDnaAggregator.getCalcVenueDna();

        assertEquals(calcVenueDna.downvotedDna.counter.longValue(), 0);
        assertEquals(calcVenueDna.upvotedDna.counter.longValue(), 1833);

        assertEquals(calcVenueDna.upvotedDna.ageId, 0);
        assertEquals(calcVenueDna.upvotedDna.spendingLimit, 0);

        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.musicPreferences.get(0).count, 5500);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).dnaId, 0);
        assertEquals(calcVenueDna.upvotedDna.atmospherePreferences.get(0).count, 5500);
    }

    /*
     * Helper Functions
     */

    private void upvoteVenue(UserDna userDna) {
        coreData.setUserDna(userDna.userId, userDna);
        addUpvoteToUserActivity(userDna.userId, venueActivity.id);
//        assertTrue(userActivityMap.get(userDna.userId).upvotes.contains(venueActivity.id));
        venueActivity.upvotes.add(userDna.userId);
    }

    private void downvoteVenue(UserDna userDna) {
        coreData.setUserDna(userDna.userId, userDna);
        addDownvoteToUserActivity(userDna.userId, venueActivity.id);
//        assertTrue(userActivityMap.get(userDna.userId).downvotes.contains(venueActivity.id));
        venueActivity.downvotes.add(userDna.userId);
    }

    private void addUpvoteToUserActivity(Long userId, Long venueId) {
        UserActivity activity = getUserActivity(userId);
        activity.downvotes.remove(venueId);
        activity.upvotes.add(venueId);
        coreData.setUserActivity(activity);
    }

    private void addDownvoteToUserActivity(Long userId, Long venueId) {
        UserActivity activity = getUserActivity(userId);
        activity.upvotes.remove(venueId);
        activity.downvotes.add(venueId);
        coreData.setUserActivity(activity);
    }

    private UserActivity getUserActivity(Long userId) {
        UserActivity activity = userActivityMap.get(userId);
        if (activity == null) {
            activity = new UserActivity(userId);
            userActivityMap.put(userId, activity);
        }
        return activity;
    }
}
