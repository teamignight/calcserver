import com.ignight.calcs.UserToUserDna;

import com.ignight.data.UserDna;
import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class UserToUserDnaTest extends TestCase {
    private final static Logger log = LoggerFactory.getLogger(UserToUserDnaTest.class);
    UserDna dnaOne = new UserDna();
    UserDna dnaTwo = new UserDna();

    public void setUp() {
        dnaOne.cityId = 0l;
        dnaOne.userId = 1l;
        dnaOne.ageId = 1l;
        dnaOne.spendingLimit = 2l;
        dnaOne.musicIds.add(1l);
        dnaOne.atmosphereIds.add(1l);

        dnaTwo.cityId = 0l;
        dnaTwo.userId = 2l;
        dnaTwo.ageId = 1l;
        dnaTwo.spendingLimit = 2l;
        dnaTwo.musicIds.add(1l);
        dnaTwo.atmosphereIds.add(1l);
    }

    public void testUserToUserCase() {
        dnaOne.ageId = 2l;
        dnaOne.spendingLimit = 1l;
    }

    public void testSpendingCompatibility() {
        assertEquals(0.0, UserToUserDna.calculateSpendingCompatibility(1, 3));
        assertEquals(0.0, UserToUserDna.calculateSpendingCompatibility(3, 1));
        assertEquals(0.5, UserToUserDna.calculateSpendingCompatibility(1, 2));
        assertEquals(0.5, UserToUserDna.calculateSpendingCompatibility(2, 1));
        assertEquals(1.0, UserToUserDna.calculateSpendingCompatibility(0, 0));
    }

    public void testAgeCompatibility() {
        assertEquals(0.0, UserToUserDna.calculateAgeCompatibility(3, 1));
        assertEquals(0.0, UserToUserDna.calculateAgeCompatibility(1, 3));
        assertEquals(0.5, UserToUserDna.calculateAgeCompatibility(2, 1));
        assertEquals(0.5, UserToUserDna.calculateAgeCompatibility(1, 2));
        assertEquals(1.0, UserToUserDna.calculateAgeCompatibility(1, 1));
    }

    public void testMusicAtmCompatibility() {
        ArrayList<Long> userOne = new ArrayList<Long>();
        ArrayList<Long> userTwo = new ArrayList<Long>();
        userOne.add(1l);
        userTwo.add(1l);
        assertEquals(1.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(1l);
        userOne.add(2l); userTwo.add(2l);
        assertEquals(1.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(1l);
        userOne.add(2l); userTwo.add(2l);
        userOne.add(3l); userTwo.add(3l);
        assertEquals(1.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(3l);
        assertNotSame(1.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(2l);
        userOne.add(2l); userTwo.add(1l);
        assertEquals(1.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(2l); userTwo.add(1l);
        userOne.add(1l); userTwo.add(2l);
        assertEquals(1.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(3l);
        userOne.add(2l); userTwo.add(2l);
        userOne.add(3l); userTwo.add(1l);
        assertEquals(0.65, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(4l);
        userOne.add(2l); userTwo.add(5l);
        userOne.add(3l); userTwo.add(6l);
        assertEquals(0.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(4l);
        userOne.add(2l);
        userOne.add(3l);
        assertEquals(0.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(3l);
        userOne.add(2l);
        userOne.add(3l);
        assertEquals(0.45, UserToUserDna.calculateCompatibility(userOne, userTwo), 0.01);
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(2l);
        userOne.add(2l);
        userOne.add(3l);
        assertEquals(0.85, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());

        userOne.clear(); userTwo.clear();
        userOne.add(1l); userTwo.add(1l);
        userOne.add(2l);
        userOne.add(3l);
        assertEquals(1.0, UserToUserDna.calculateCompatibility(userOne, userTwo));
        assertEquals(userOne.size(), userTwo.size());
    }

    public void testUserDnaInternals() {
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo));
        assertEquals(1, dnaOne.musicIds.size());
        assertEquals(1, dnaTwo.musicIds.size());
        assertEquals(1, dnaOne.atmosphereIds.size());
        assertEquals(1, dnaTwo.atmosphereIds.size());
    }

    public void testAgeUserToUserDnaCompatibility() {
        dnaOne.ageId = 0l; dnaTwo.ageId = 1l;
        assertEquals(0.925, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
        dnaOne.ageId = 0l; dnaTwo.ageId = 2l;
        assertEquals(0.85, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
        dnaOne.ageId = 0l; dnaTwo.ageId = 3l;
        assertEquals(0.85, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
    }

    public void testSpendingUserToUserDnaCompatibiltiy() {
        dnaOne.spendingLimit = 0l; dnaTwo.spendingLimit = 1l;
        assertEquals(0.925, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
        dnaOne.spendingLimit = 0l; dnaTwo.spendingLimit = 2l;
        assertEquals(0.85, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
        dnaOne.spendingLimit = 0l; dnaTwo.spendingLimit = 3l;
        assertEquals(0.85, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
    }

    public void testMusicUserToUserDnaCompatibility() {
        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(2l);
        assertEquals(0.5, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l); dnaTwo.musicIds.add(3l);
        assertEquals(0.975, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l); dnaTwo.musicIds.add(2l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l); dnaTwo.musicIds.add(2l);
        dnaOne.musicIds.add(3l); dnaTwo.musicIds.add(4l);
        assertEquals(0.925, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l);
        dnaOne.musicIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateCompatibility(dnaOne.musicIds, dnaTwo.musicIds));
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
                                 dnaTwo.musicIds.add(2l);
                                 dnaTwo.musicIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(3l); dnaTwo.musicIds.add(2l);
                                 dnaTwo.musicIds.add(3l);
        assertEquals(0.9, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l); dnaTwo.musicIds.add(3l);
        dnaOne.musicIds.add(3l);
        assertEquals(0.9, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l); dnaTwo.musicIds.add(2l);
                                 dnaTwo.musicIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l); dnaTwo.musicIds.add(2l);
        dnaOne.musicIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
        dnaOne.musicIds.add(2l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(1l);
                                 dnaTwo.musicIds.add(2l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(1l); dnaTwo.musicIds.add(2l);
        dnaOne.musicIds.add(2l);
        assertEquals(0.925, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.musicIds.clear(); dnaTwo.musicIds.clear();
        dnaOne.musicIds.add(2l); dnaTwo.musicIds.add(1l);
                                 dnaTwo.musicIds.add(2l);
        assertEquals(0.925, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
    }

    public void testAtmosphereUserToUserDnaCompatibility() {
        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(2l);
        assertEquals(0.8, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l); dnaTwo.atmosphereIds.add(3l);
        assertEquals(0.99, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l); dnaTwo.atmosphereIds.add(2l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l); dnaTwo.atmosphereIds.add(2l);
        dnaOne.atmosphereIds.add(3l); dnaTwo.atmosphereIds.add(4l);
        assertEquals(0.97, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l);
        dnaOne.atmosphereIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
                                      dnaTwo.atmosphereIds.add(2l);
                                      dnaTwo.atmosphereIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(3l); dnaTwo.atmosphereIds.add(2l);
                                      dnaTwo.atmosphereIds.add(3l);
        assertEquals(0.96, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l); dnaTwo.atmosphereIds.add(3l);
        dnaOne.atmosphereIds.add(3l);
        assertEquals(0.96, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l); dnaTwo.atmosphereIds.add(2l);
                                      dnaTwo.atmosphereIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l); dnaTwo.atmosphereIds.add(2l);
        dnaOne.atmosphereIds.add(3l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
        dnaOne.atmosphereIds.add(2l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(1l);
                                      dnaTwo.atmosphereIds.add(2l);
        assertEquals(1.0, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(1l); dnaTwo.atmosphereIds.add(2l);
        dnaOne.atmosphereIds.add(2l);
        assertEquals(0.97, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);

        dnaOne.atmosphereIds.clear(); dnaTwo.atmosphereIds.clear();
        dnaOne.atmosphereIds.add(2l); dnaTwo.atmosphereIds.add(1l);
                                      dnaTwo.atmosphereIds.add(2l);
        assertEquals(0.97, UserToUserDna.calculateUserToUserDna(dnaOne, dnaTwo), 0.001);
    }
}
