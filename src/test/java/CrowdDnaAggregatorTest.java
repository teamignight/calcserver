import com.ignight.calcs.CrowdDnaAggregator;
import com.ignight.data.TrendingValues;
import com.ignight.data.UserDna;
import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CrowdDnaAggregatorTest extends TestCase {
    private static final Logger log = LoggerFactory.getLogger(CrowdDnaAggregatorTest.class);

    private CrowdDnaAggregator aggregator;

    public void setUp() {
        aggregator = new CrowdDnaAggregator();
    }

    public void testDetermineMax() {
        Map<Long, Long> values = new HashMap<Long, Long>();
        values.put(1l, 10l);
        assertEquals(1l, aggregator.determineMax(values));

        values.put(0l, 9l);
        assertEquals(1l, aggregator.determineMax(values));

        values.put(0l, 11l);
        assertEquals(0l, aggregator.determineMax(values));
    }

    public void testSingleDnaOneChoice() {
        UserDna dna = new UserDna(1l).ageId(0l).musicId(0l).atmosphereId(0l).spendingLimit(0l);
        aggregator.apply(dna);
        TrendingValues actual = aggregator.getCollectionDna();
        TrendingValues expected = new TrendingValues(0, 0, 0, 0);
        validateCollection(actual, expected);

        expected = new TrendingValues(1, 2, 3, 4);
        invalidateCollection(actual, expected);
    }

    public void testSingleDnaMultipleChoice() {
        UserDna dna = new UserDna(1l).ageId(0l).musicId(0l).musicId(1l).atmosphereId(0l).atmosphereId(1l).spendingLimit(0l);
        aggregator.apply(dna);

        TrendingValues actual = aggregator.getCollectionDna();
        TrendingValues expected = new TrendingValues(0, 0, 0, 0);
        validateCollection(actual, expected);

        aggregator.clear();
        dna.musicId(2l).atmosphereId(2l);
        aggregator.apply(dna);

        actual = aggregator.getCollectionDna();
        expected = new TrendingValues(0, 0, 0, 0);
        validateCollection(actual, expected);
    }

    public void testMultipleDna() {
        UserDna dna = new UserDna(1l).ageId(0l).musicId(0l).musicId(1l).musicId(2l)
                .atmosphereId(0l).atmosphereId(1l).atmosphereId(2l).spendingLimit(0l);
        aggregator.apply(dna);

        // age - 0 (1)
        // music - 0 (3), 1(2), 2(1)
        // atmosphere - 0 (3), 1(2), 2(1)
        // spending - 0 (1)
        long val = aggregator.ageIdCounter.get(0l);
        assertEquals(1l, val);

        val = aggregator.spendingIdCounter.get(0l);
        assertEquals(1l, val);

        val = aggregator.atmosphereIdCounter.get(0l);
        assertEquals(3l, val);
        val = aggregator.atmosphereIdCounter.get(1l);
        assertEquals(2l, val);
        val = aggregator.atmosphereIdCounter.get(2l);
        assertEquals(1l, val);

        val = aggregator.musicIdCounter.get(0l);
        assertEquals(3l, val);
        val = aggregator.musicIdCounter.get(1l);
        assertEquals(2l, val);
        val = aggregator.musicIdCounter.get(2l);
        assertEquals(1l, val);

        UserDna dna2 = new UserDna(2l).ageId(0l)
                .musicId(3l).musicId(2l).musicId(1l)
                .atmosphereId(3l).atmosphereId(2l).atmosphereId(1l)
                .spendingLimit(0l);
        aggregator.apply(dna2);

        // age - 0(2)
        // music - 0(3), 1(3), 2(3), 3(3)
        // atmosphere - 0(3), 1(3), 2(3), 3(3)
        // spending - 0(2)

        val = aggregator.ageIdCounter.get(0l);
        assertEquals(2l, val);

        val = aggregator.spendingIdCounter.get(0l);
        assertEquals(2l, val);

        val = aggregator.atmosphereIdCounter.get(0l);
        assertEquals(3l, val);
        val = aggregator.atmosphereIdCounter.get(1l);
        assertEquals(3l, val);
        val = aggregator.atmosphereIdCounter.get(2l);
        assertEquals(3l, val);
        val = aggregator.atmosphereIdCounter.get(3l);
        assertEquals(3l, val);

        val = aggregator.musicIdCounter.get(0l);
        assertEquals(3l, val);
        val = aggregator.musicIdCounter.get(1l);
        assertEquals(3l, val);
        val = aggregator.musicIdCounter.get(2l);
        assertEquals(3l, val);
        val = aggregator.musicIdCounter.get(3l);
        assertEquals(3l, val);

        UserDna dna3 = new UserDna(3l).ageId(1l).musicId(2l).musicId(3l).musicId(10l)
                .atmosphereId(0l).atmosphereId(1l).atmosphereId(2l)
                .spendingLimit(1l);
        aggregator.apply(dna3);

        // age - 0(2), 1(1)
        // music - 0(3), 1(3), 2(6), 3(5), 10(1)
        // atmosphere - 0(6), 1(5), 2(4), 3(3)
        // spending - 0(2), 1(1)

        TrendingValues actual = aggregator.getCollectionDna();
        TrendingValues expeted = new TrendingValues(0, 2, 0, 0);

        assertNotNull(aggregator.ageIdCounter.get(0l));
        val = aggregator.ageIdCounter.get(0l);
        assertEquals(2l, val);
        val = aggregator.ageIdCounter.get(1l);
        assertEquals(1l, val);

        val = aggregator.spendingIdCounter.get(0l);
        assertEquals(2l, val);
        val = aggregator.spendingIdCounter.get(1l);
        assertEquals(1l, val);

        val = aggregator.atmosphereIdCounter.get(0l);
        assertEquals(6l, val);
        val = aggregator.atmosphereIdCounter.get(1l);
        assertEquals(5l, val);
        val = aggregator.atmosphereIdCounter.get(2l);
        assertEquals(4l, val);
        val = aggregator.atmosphereIdCounter.get(3l);
        assertEquals(3l, val);

        val = aggregator.musicIdCounter.get(0l);
        assertEquals(3l, val);
        val = aggregator.musicIdCounter.get(1l);
        assertEquals(3l, val);
        val = aggregator.musicIdCounter.get(2l);
        assertEquals(6l, val);
        val = aggregator.musicIdCounter.get(3l);
        assertEquals(5l, val);
        val = aggregator.musicIdCounter.get(10l);
        assertEquals(1l, val);

        validateCollection(actual, expeted);
    }

    private void validateCollection(TrendingValues actual, TrendingValues expected) {
        assertEquals(expected.ageId, actual.ageId);
        assertEquals(expected.spendingId, actual.spendingId);
        assertEquals(expected.musicId, actual.musicId);
        assertEquals(expected.atmosphereId, actual.atmosphereId);
    }

    private void invalidateCollection(TrendingValues actual, TrendingValues expected) {
        assertNotSame(expected.ageId, actual.ageId);
        assertNotSame(expected.spendingId, actual.spendingId);
        assertNotSame(expected.musicId, actual.musicId);
        assertNotSame(expected.atmosphereId, actual.atmosphereId);
    }
}
