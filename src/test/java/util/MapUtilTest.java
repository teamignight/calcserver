package util;

import java.util.*;

import com.ignight.util.MapUtil;
import org.junit.*;

public class MapUtilTest
{
    @Test
    public void testSortByValueAscending()
    {
        Random random = new Random(System.currentTimeMillis());
        Map<String, Integer> testMap = new HashMap<String, Integer>(1000);
        for(int i = 0 ; i < 1000 ; ++i) {
            testMap.put( "SomeString" + random.nextInt(), random.nextInt());
        }

        testMap = MapUtil.sortByValue(testMap, false);
        Assert.assertEquals( 1000, testMap.size() );

        Integer previous = null;
        for(Map.Entry<String, Integer> entry : testMap.entrySet()) {
            Assert.assertNotNull( entry.getValue() );
            if (previous != null) {
                Assert.assertTrue( entry.getValue() >= previous );
            }
            previous = entry.getValue();
        }
    }

    @Test
    public void testSortByValueDescending()
    {
        Random random = new Random(System.currentTimeMillis());
        Map<Integer, Integer> testMap = new HashMap<Integer, Integer>(1000);
        for(int i = 0 ; i < 1000 ; ++i) {
            testMap.put( random.nextInt(), random.nextInt());
        }

        testMap = MapUtil.sortByValue(testMap, true);
        Assert.assertEquals( 1000, testMap.size() );

        Integer previous = null;
        for(Map.Entry<Integer, Integer> entry : testMap.entrySet()) {
            Assert.assertNotNull( entry.getValue() );
            if (previous != null) {
                Assert.assertTrue( entry.getValue() <= previous );
            }
            previous = entry.getValue();
        }
    }
}