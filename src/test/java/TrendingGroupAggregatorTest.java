import com.ignight.calcs.TrendingGroupAggregator;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TrendingGroupAggregatorTest extends TestCase {
   TrendingGroupAggregator aggregator;

    public void setUp() {
        aggregator = new TrendingGroupAggregator();
    }

    public void testSingleGroupSingleUser() {
        Set<Long> groups = createGroupsSet(1);

        aggregator.apply(groups);
        List<TrendingGroupAggregator.TrendingGroup> trendingGroups = aggregator.determineTopTrending();
        assertEquals(1l, trendingGroups.size());
        TrendingGroupAggregator.TrendingGroup trendingGroup = trendingGroups.get(0);
        assertEquals(1l, trendingGroup.groupId.intValue());
        assertEquals(1, trendingGroup.count.intValue());
    }

    public void testMultipleGroupsSingleUser() {
        Set<Long> groups = createGroupsSet(1, 2, 3);
        aggregator.apply(groups);

        List<TrendingGroupAggregator.TrendingGroup> trendingGroups = aggregator.determineTopTrending();
        assertEquals(3l, trendingGroups.size());

        TrendingGroupAggregator.TrendingGroup trendingGroup = trendingGroups.get(0);
        assertEquals(1, trendingGroup.groupId.intValue());
        assertEquals(1, trendingGroup.count.intValue());

        trendingGroup = trendingGroups.get(1);
        assertEquals(2, trendingGroup.groupId.intValue());
        assertEquals(1, trendingGroup.count.intValue());

        trendingGroup = trendingGroups.get(2);
        assertEquals(3, trendingGroup.groupId.intValue());
        assertEquals(1, trendingGroup.count.intValue());
    }

    public void testSingleGroupMultipleUsers() {
        Set<Long> groups = createGroupsSet(1);
        aggregator.apply(groups);
        aggregator.apply(groups);
        aggregator.apply(groups);

        List<TrendingGroupAggregator.TrendingGroup> trendingGroups = aggregator.determineTopTrending();
        assertEquals(1l, trendingGroups.size());
        TrendingGroupAggregator.TrendingGroup trendingGroup = trendingGroups.get(0);
        assertEquals(1l, trendingGroup.groupId.intValue());
        assertEquals(3, trendingGroup.count.intValue());
    }

    public void testMultipleGroupsMultipleUsers() {
        Set<Long> groups = createGroupsSet(1, 2, 3, 10, 20);
        aggregator.apply(groups);
        groups = createGroupsSet(2, 3, 4, 10, 40);
        aggregator.apply(groups);
        groups = createGroupsSet(1, 2, 3, 10, 40);
        aggregator.apply(groups);

        /* Results
         *  Group 1 : 2
         *  Group 2 : 3
         *  Group 3 : 3
         *  Group 4 : 1
         *  Group 10 : 3
         *  Group 20 : 1
         *  Group 40 : 2
         */

        List<TrendingGroupAggregator.TrendingGroup> trendingGroups = aggregator.determineTopTrending();
        assertEquals(3l, trendingGroups.size());

        TrendingGroupAggregator.TrendingGroup trendingGroup = trendingGroups.get(0);
        assertEquals(2, trendingGroup.groupId.intValue());
        assertEquals(3, trendingGroup.count.intValue());

        trendingGroup = trendingGroups.get(1);
        assertEquals(3, trendingGroup.groupId.intValue());
        assertEquals(3, trendingGroup.count.intValue());

        trendingGroup = trendingGroups.get(2);
        assertEquals(10, trendingGroup.groupId.intValue());
        assertEquals(3, trendingGroup.count.intValue());
    }

    public Set<Long> createGroupsSet(long ... groups) {
        Set<Long> groupList = new HashSet<Long>();
        for (long g : groups) {
            groupList.add(g);
        }
        return groupList;
    }
}
