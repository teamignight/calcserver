package com.ignight.calcserver;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.ignight.core.CalcVenueDnaCalc;
import com.ignight.core.CoreData;

import com.ignight.databases.JedisConnection;
import com.ignight.databases.RedisKeys;
import com.ignight.loaders.UserDataLoader;
import com.ignight.sqs.SQSMessageListener;
import com.ignight.util.CalcProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CalcServer {
    private static final Logger log = LoggerFactory.getLogger(CalcServer.class);
    protected static final JedisConnection jedis = JedisConnection.getInstance();

    public static void main(String [] args) throws Exception {
        try {
            log.info("Starting CalcServer : {}", InetAddress.getLocalHost().getHostName());
            log.info("Available Runtime Processors: {}", Runtime.getRuntime().availableProcessors());

            final ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

            String sqsQueueName = InetAddress.getLocalHost().getHostName().replace('.', '-');
            final String redisQueueKey = RedisKeys.SQS_MSG_QUEUE_PREFIX + sqsQueueName;
            Long now = System.currentTimeMillis();
            jedis.setex(redisQueueKey, now.toString(), 30);

            /*
             * Load UserDnas
             */
            CoreData.venueSize = jedis.keys(RedisKeys.VENUE_HASH_KEY_PREFIX + "*").size();

            log.info("Loading User DNA & Venue Values");
            Future future = executorService.submit(new UserDataLoader());
            while(!future.isDone());
            log.info("Loaded User Dna & Venue Values");

            /*
             * Start SQS Listener
             */
            ProfileCredentialsProvider profileCredentialsProvider = new ProfileCredentialsProvider();
            AWSCredentials awsCredentials = profileCredentialsProvider.getCredentials();

            Region region = Region.getRegion(Regions.US_EAST_1);
            AmazonSQS sqs = new AmazonSQSClient(awsCredentials);
            sqs.setRegion(region);

            CreateQueueRequest createQueueRequest = new CreateQueueRequest(CalcProperties.getSqsQueue());
            String defaultQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();

            createQueueRequest = new CreateQueueRequest(sqsQueueName);
            String queueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
            final SQSMessageListener sqsMessageListener = new SQSMessageListener(redisQueueKey, awsCredentials, region, queueUrl, defaultQueueUrl);
            executorService.execute(sqsMessageListener);

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    log.info("Inside Add Shutdown Hook");
                    jedis.del(redisQueueKey);
                    sqsMessageListener.shutdown();
                    log.info("Shutting Down CalcServer...");
                }
            });

            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            executor.scheduleAtFixedRate(new CalcVenueDnaCalc(),
                    CalcProperties.getVenueDnaThreadInitialDelayMillis(),
                    CalcProperties.getVenueDnaThreadPeriodMillis(),
                    TimeUnit.MILLISECONDS);
//            executorService.execute(new UpdateUserValues());
        } catch (Exception e) {
            log.error("Failed to start CalcServer", e);
        }
    }
}