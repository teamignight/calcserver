package com.ignight.sqs;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.calcs.GroupCalc;
import com.ignight.calcs.VenueDnaCalc;
import com.ignight.core.CoreData;
import com.ignight.data.UserActivity;
import com.ignight.databases.JedisConnection;
import com.ignight.messages.QueueMessage;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class SQSMessageListener implements Runnable {
    private static Logger log = LoggerFactory.getLogger(SQSMessageListener.class);
    private CoreData coreData = CoreData.getInstance();
    private static AmazonSQSClient client;
    private String redisQueueKey;
    private String queueUrl;

    private Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    private ExecutorService executors = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private JedisConnection jedis = JedisConnection.getInstance();
    private String defaultQueueUrl;
    private boolean shutdown = false;

    private ArrayList<Future> futures = new ArrayList<Future>();

    public SQSMessageListener(String redisQueueKey, AWSCredentials credentials, Region region, String queueUrl, String defaultQueueUrl) {
        this.redisQueueKey = redisQueueKey;
        this.client = new AmazonSQSClient(credentials);
        this.client.setRegion(region);
        this.queueUrl = queueUrl;
        this.defaultQueueUrl = defaultQueueUrl;

        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(80), 0);
            server.createContext("/ping", new MyHandler());
            server.setExecutor(null); // creates a default executor
            server.start();
        } catch (IOException e) {
            log.error("Failed to start Health Check HttpServer", e);
        }
    }

    static class MyHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            String response = "Success";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    public void shutdown() {
        shutdown = true;
    }

    public void run() {
        while (true) {
            try {
                int maxNumberOfMessages = 10;

                ReceiveMessageRequest request = new ReceiveMessageRequest();
                request.setWaitTimeSeconds(20);
                request.setMaxNumberOfMessages(maxNumberOfMessages);

                String currentQueueUrl;
                if (defaultQueueUrl == null) {
                    currentQueueUrl = queueUrl;
                } else {
                    currentQueueUrl = defaultQueueUrl;
                }
                request.setQueueUrl(currentQueueUrl);
                List<Message> messages = client.receiveMessage(request).getMessages();
                if (!shutdown) {
                    log.debug("Got {} messages from queue {} ", messages.size(), request.getQueueUrl());
                    for (Message message : messages) {
                        processMessage(message, currentQueueUrl);
                    }
                    //Set default queue url to null once all messages from it have been processed
                    if (messages.size() == 0) {
                        defaultQueueUrl = null;
                    }
                    jedis.setex(redisQueueKey, ((Long) System.currentTimeMillis()).toString(), 30);
                } else {
                    //Process All Current Messages In Queue Before Shutting Down
                    jedis.del(redisQueueKey);
                    while (messages.size() > 0) {
                        for (Message message : messages) {
                            processMessage(message, currentQueueUrl);
                        }
                        messages = client.receiveMessage(request).getMessages();
                    }
                }

                //Wait for threads to finish or 30 seconds before terminating
                if (shutdown) {
                    executors.awaitTermination(60, TimeUnit.SECONDS);
                    break;
                }
            } catch (InterruptedException e) {
                log.error("Failed to shutdown gracefully: {}", e.getMessage(), e);
            } catch (AmazonServiceException e) {
                log.error("Rejected by SQS : {}", e.getMessage(), e);
            } catch (AmazonClientException e) {
                log.error("Failed to communicate with SQS : {}", e.getMessage(), e);
            } catch (Exception e) {
                log.error("SQSMessageListener failed: {}", e.getMessage(), e);
            }
        }
    }

    private void processMessage(Message message, String currentQueueUrl) {
        String body = null;
        try {
            body = message.getBody();
            log.info("Processing message: {}", body);
            QueueMessage m = gson.fromJson(body, QueueMessage.class);

            switch (m.type) {
                case ADD_ACTIVITY:
                case REMOVE_ACTIVITY:
                case SWITCH_TO_REMOVE:
                case SWITCH_TO_ADD:
                    executors.submit(new VenueDnaCalc(m));
                    UserActivity userActivity = coreData.getUserActivity(m.userId);
                    coreData.addVenueActivity(m, userActivity);
                    break;
                case NEW_USER:
                case DNA_CHANGE:
                    coreData.loadUserDna(m.userId);
                    UserActivity newUserActivity = coreData.getUserActivity(m.userId);
                    coreData.addVenueActivity(null, newUserActivity);
                    break;
                case NEW_PUBLIC_GROUP:
                case NEW_PRIVATE_GROUP:
                case JOIN_GROUP:
                case LEAVE_GROUP:
                    executors.submit(new GroupCalc(m));
                    break;
                case RESET:
                    executors.submit(new GroupCalc(m));
                    break;
                case CLEAR : {
                    coreData.clear();
                    break;
                }
                default:
                    log.error("Unknown Event Type: {}", m.type);
            }
        } catch (Exception e) {
            log.error("Failed to process message: {}", body, e);
        } finally {
            //TODO: Change this to do a batch delete?
            //Delete message
            client.deleteMessage(currentQueueUrl, message.getReceiptHandle());
        }
    }
}
