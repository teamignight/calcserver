package com.ignight.calcs;

import com.ignight.data.TrendingValues;
import com.ignight.data.UserDna;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CrowdDnaAggregator {
    private static final Logger log = LoggerFactory.getLogger(CrowdDnaAggregator.class);
    public Map<Long, Long> ageIdCounter = new HashMap<Long, Long>();
    public Map<Long, Long> musicIdCounter = new HashMap<Long, Long>();
    public Map<Long, Long> atmosphereIdCounter = new HashMap<Long, Long>();
    public Map<Long, Long> spendingIdCounter = new HashMap<Long, Long>();

    public void apply(UserDna dna) {
        if (dna != null) {
            applyToPart(ageIdCounter, dna.ageId, 1l);
            applyToPart(spendingIdCounter, dna.spendingLimit, 1l);
            applyToPart(musicIdCounter, dna.musicIds);
            applyToPart(atmosphereIdCounter, dna.atmosphereIds);
        }
    }

    public TrendingValues getCollectionDna() {
        return new TrendingValues(determineMax(ageIdCounter),
                determineMax(musicIdCounter), determineMax(atmosphereIdCounter),
                determineMax(spendingIdCounter));
    }

    public void clear() {
        ageIdCounter.clear();
        musicIdCounter.clear();
        atmosphereIdCounter.clear();
        spendingIdCounter.clear();
    }

    public static long determineMax(Map<Long, Long> values) {
        long maxId = -1l;
        long maxCount = -1l;
        for (Map.Entry<Long, Long> entry : values.entrySet()) {
            if (entry.getValue() > maxCount) {
                maxCount = entry.getValue();
                maxId = entry.getKey();
            }
        }
        return maxId;
    }

    private static void applyToPart(Map<Long, Long> dnaPiece, List<Long> ids) {
        for (int i = 0; i < ids.size(); i++) {
            Long id = ids.get(i);
            if (id >= 0l) {
                applyToPart(dnaPiece, id, getPosScore(i));
            }
        }
    }

    private static long getPosScore(int pos) {
        if (pos == 0) {
            return 3;
        } else if (pos == 1) {
            return 2;
        } else if (pos == 2) {
            return 1;
        }
        return 0;
    }

    private static void applyToPart(Map<Long, Long> dnaPiece, Long id, Long score) {
        Long count = dnaPiece.get(id);
        if (count == null) {
            dnaPiece.put(id, score);
        } else {
            dnaPiece.put(id, count + score);
        }
    }
}
