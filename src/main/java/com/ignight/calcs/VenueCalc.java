package com.ignight.calcs;

import com.ignight.data.TrendingValues;
import com.ignight.data.UserDna;
import com.ignight.data.VenueActivity;
import com.ignight.data.VenueTrending;
import com.ignight.databases.RedisKeys;
import com.ignight.messages.QueueMessage;
import com.ignight.messages.QueueMessageType;
import com.ignight.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/*
 This is a full Venue Calc For All Users
 */
@Deprecated
public class VenueCalc extends Calc {
    private static final Logger log = LoggerFactory.getLogger(VenueCalc.class);

    public Timer timer = new Timer();
    public static String TOTAL_TIME = "Total Time";
    public static String GET_USER_DNA = "Get User DNA";
    public static String GET_ITER_USER_DNA = "Get Iter User DNA";
    public static String GET_USER_ACTIVITY = "Get User Activity";
    public static String GET_USER_TO_USER_DNA = "Get User To User DNA";
    public static String GET_REDIS_VAL = "Get Redis Val";
    public static String ADJUST_VAL = "Adjust Venue Dna Val";
    public static String UPDATE_REDIS_VAL = "Update Redis Val";
    public static String UPDATE_DELTA = "Update delta";
    public static Long THREAD_ID = Thread.currentThread().getId();

    public VenueCalc(QueueMessage message) {
        super(message);
        log.info("Beginning Venue Calc: {}", message);
    }

    @Override
    public void run() {
        try {
            timer.beginTime(TOTAL_TIME);

            timer.beginTime(GET_USER_ACTIVITY);
            /*
             Iterate through all the users and adjust the venue dna
             based on delta for each user-venue value
             */
            UserDna userDna = coreData.getUserDna(message.userId);
            if (userDna == null) {
                log.error("User does not exists (yet?) : {}", message.userId);
                return;
            }

            //Get User Activity
            Set<Long> userActivity = getUserActivity(message.userId);
            Set<Long> upvotes = new HashSet<Long>();
            Set<Long> downvotes = new HashSet<Long>();
            separateActivity(userActivity, upvotes, downvotes);
            timer.endTime(GET_USER_ACTIVITY);

            log.info("User Activity: Upvotes: {}, Downvotes: {}", upvotes, downvotes);

            Iterator<Long> usersIter = coreData.getUserIds().iterator();
            while (usersIter.hasNext()) {
                Long userId = usersIter.next();
                if (userId.equals(userDna.userId)) {
                    continue;
                }
                timer.beginTime(GET_USER_TO_USER_DNA);
                //Calculate user to user dna
                UserDna iterUser = coreData.getUserDna(userId);
                Double userToUserDna = 0d;
                timer.endTime(GET_USER_TO_USER_DNA);

                timer.beginTime(UPDATE_DELTA);
                if (upvotes.contains(message.typeId)) {
                    if (message.type == QueueMessageType.ADD_ACTIVITY) {    //Simple Upvote, was neutral
                        adjustUpvotedVenueScoresOnUpvote(upvotes, userId, userToUserDna);
                    } else if (message.type == QueueMessageType.SWITCH_TO_ADD) {    //Switched, was negative
                        adjustDownvotedVenueScoresOnUpvote(downvotes, userId, userToUserDna);
                        adjustUpvotedVenueScoresOnUpvote(upvotes, userId, userToUserDna);
                    }
                } else if (downvotes.contains(message.typeId)) {
                    if (message.type == QueueMessageType.REMOVE_ACTIVITY) { //Simple Downvote, was neutral
                        adjustDownvotedVenueScoresOnDownvote(downvotes, userId, userToUserDna);
                    } else if (message.type == QueueMessageType.SWITCH_TO_REMOVE) { //Switched, was positive
                        adjustUpvotedVenueScoresOnDownvote(upvotes, userId, userToUserDna);
                        adjustDownvotedVenueScoresOnDownvote(downvotes, userId, userToUserDna);
                    }
                } else {
                    if (message.type == QueueMessageType.ADD_ACTIVITY) { // Removed downvote
                        adjustDownvotedVenueScoresOnUpvote(downvotes, userId, userToUserDna);
                    } else if (message.type == QueueMessageType.REMOVE_ACTIVITY) {  // Removed upvote
                        adjustUpvotedVenueScoresOnDownvote(upvotes, userId, userToUserDna);
                    }
                }
                timer.endTime(UPDATE_DELTA);
            }
            timer.endTime(TOTAL_TIME);
            log.info("calcTime: \n{}", timer.print());

            log.debug("---------------------------------------------");

            long begin  = System.currentTimeMillis();
            VenueActivity activity = coreData.getVenueActivity(message.typeId);
            if (activity == null) {
                log.error("Activity should never be null since core data was updated right before this");
                return;
            }

            /*
             * -- Calculate Venue Dna ---
             * 1. Iterate through each up-voted user
             * 2. Add users dna values
             * 3. Get users groups and aggregate
             * 4. Get VenueDna and Trending Groups from Aggregators
             * 5. Save to redis
             */

            Iterator<Long> upvoteIter = activity.upvotes.iterator();
            CrowdDnaAggregator aggregator = new CrowdDnaAggregator();
            TrendingGroupAggregator groupAggregator = new TrendingGroupAggregator();

            while (upvoteIter.hasNext()) {
                //Get Users Dna
                Long userId = upvoteIter.next();

                UserDna dna = coreData.getUserDna(userId);
                if (dna == null) {
                    log.error("Dna for user : {} is null", userId);
                    continue;
                } else {
                    log.debug("Using User: {} , DNA: {}", userId, dna);
                }

//                log.info("Adding Dna: {}", gson.toJson(dna));
                aggregator.apply(dna);

                //Apply Users Group Values For Trending Groups at Venue
                Set<Long> userGroups = coreData.getUserGroups(userId);
                groupAggregator.apply(userGroups);
            }

            TrendingValues dna = aggregator.getCollectionDna();
            List<TrendingGroupAggregator.TrendingGroup> groups = groupAggregator.determineTopTrending();
            VenueTrending venueTrending = new VenueTrending(dna, groups);

            String key = RedisKeys.getVenueTrendingKey(message.typeId);
            log.debug("Setting VenueDna: Key={}, venueId={}, VenueDna: {}", key, message.typeId, gson.toJson(venueTrending));
            jedis.hset(key, message.typeId.toString(), venueTrending);

            log.info("calcTime: {} ms", System.currentTimeMillis() - begin);
        } catch (Exception e) {
            log.error("Failed to calculate venue calc for " + message, e);
        }
    }

    private void adjustUpvotedVenueScoresOnUpvote(Set<Long> upvotes, Long userId, Double userToUserDna) {
        for (Long venue : upvotes) {
//            Double userVenueDnaValue = getUserVenueDnaValue(userId, venue);
            double delta = 0.0;
            if (!venue.equals(message.typeId)) {
                delta -= userToUserDna / (upvotes.size() - 1.0);
            }
            delta += userToUserDna / (upvotes.size() * 1.0);
//            setUserVenueDnaValue(userId, venue, userVenueDnaValue + delta);
        }
    }

    /*
     1. User upvotes venue
     2. Calc Server Updates Trending Crowd For Venue
     3. User Asks For Trending
            a. Get List of Active Venues
            b. Get Their Venue DNA's
            c. Calculate User to Venue DNA Value
            d. Calculate list / Calculate Bar Colors
     */

    /*
     1. User upvotes venue
     2. Calc Server Updates Venue DNA Value * M Users For Every Single Up vote
            a. Get User to User DNA Value
            b. Add Value to Venue
            c. Update Venue Value In Local Cache
     3. User Asks For Trending
            a. Get List Of Venue DNA's
            b. Calculate List / Calculate Bar Colors

     */

    private void adjustDownvotedVenueScoresOnUpvote(Set<Long> downvotes, Long userId, Double userToUserDna) {
//        Double userVenueDnaValue = getUserVenueDnaValue(userId, message.typeId);
        double delta = 0.0;
        delta += userToUserDna / (downvotes.size() + 1.0);
//        setUserVenueDnaValue(userId, message.typeId, userVenueDnaValue + delta);

        for (Long venue : downvotes) {
//            userVenueDnaValue = getUserVenueDnaValue(userId, venue);
            delta = 0.0;
            delta += userToUserDna / (downvotes.size() + 1.0);
            delta -= userToUserDna / (downvotes.size() * 1.0);
//            setUserVenueDnaValue(userId, venue, userVenueDnaValue + delta);
        }
    }

    private void adjustDownvotedVenueScoresOnDownvote(Set<Long> downvotes, Long userId, Double userToUserDna) {
        for (Long venue : downvotes) {
//            Double userVenueDnaValue = getUserVenueDnaValue(userId, venue);
            double delta = 0.0;
            if (!venue.equals(message.typeId)) {
                delta += userToUserDna / (downvotes.size() - 1.0);
            }
            delta -= userToUserDna / (downvotes.size() * 1.0);
//            setUserVenueDnaValue(userId, venue, userVenueDnaValue + delta);
        }
    }

    private void adjustUpvotedVenueScoresOnDownvote(Set<Long> upvotes, Long userId, Double userToUserDna) {
//        Double userVenueDnaValue = getUserVenueDnaValue(userId, message.typeId);
        double delta = 0.0;
        delta -= userToUserDna / (upvotes.size() + 1.0);
//        setUserVenueDnaValue(userId, message.typeId, userVenueDnaValue + delta);

        for (Long venue : upvotes) {
//            userVenueDnaValue = getUserVenueDnaValue(userId, venue);
            delta = 0.0;
            delta -= userToUserDna / (upvotes.size() + 1.0);
            delta += userToUserDna / (upvotes.size() * 1.0);
//            setUserVenueDnaValue(userId, venue, userVenueDnaValue + delta);
        }
    }

    private Set<Long> getUserActivity(Long userId) {
        String key = RedisKeys.getUserActivityKey(userId);
        Set<String> activity = jedis.smembers(key);
        Set<Long> venueActivity = new HashSet<Long>();
        if (activity != null) {
            for (String s : activity) {
                venueActivity.add(Long.valueOf(s));
            }
        }
        return venueActivity;
    }

    private void separateActivity(Set<Long> activity, Set<Long> upvotes, Set<Long> downvotes) {
        for (Long a : activity) {
            if (a < 0) {
                downvotes.add(-1 * a);
            } else {
                upvotes.add(a);
            }
        }
    }

    private double getUserVenueDnaValue(Long userId, Long venueId) {
        timer.beginTime(GET_REDIS_VAL);
        String key = RedisKeys.getUserTrendingVenuesKey(userId);
        String val = jedis.hget(key, venueId.toString());
        if (val == null) {
            return 0.0;
        }
        double ret = Double.valueOf(val);
        timer.endTime(GET_REDIS_VAL);
        return ret;
    }

    private void setUserVenueDnaValue(Long userId, Long venueId, Double value) {
        timer.beginTime(UPDATE_REDIS_VAL);
        String key = RedisKeys.getUserTrendingVenuesKey(userId);
        jedis.hset(key, venueId.toString(), value);
        timer.endTime(UPDATE_REDIS_VAL);
    }

    private double getActivityUserValueForOtherUser(double userToUserDna) {
        if (message.type == QueueMessageType.ADD_ACTIVITY) {
            return userToUserDna;
        }

        if (message.type == QueueMessageType.REMOVE_ACTIVITY) {
            return -1 * userToUserDna;
        }

        if (message.type == QueueMessageType.SWITCH_TO_ADD) {
            return 2 * userToUserDna;
        }

        if (message.type == QueueMessageType.SWITCH_TO_REMOVE) {
            return -2 * userToUserDna;
        }
        return 0;
    }
}
