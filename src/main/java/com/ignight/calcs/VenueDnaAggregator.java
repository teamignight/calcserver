package com.ignight.calcs;

import com.ignight.core.CoreData;
import com.ignight.data.CalcVenueDna;
import com.ignight.data.DnaElementCount;
import com.ignight.data.UserActivity;
import com.ignight.data.UserDna;
import com.ignight.data.VenueActivity;
import com.ignight.data.VenueDna;
import com.ignight.util.MapUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VenueDnaAggregator {
    private VenueActivity activity;
    private CoreData coreData;
    private Map<Long, UserActivity> userActivityMap;

    private static final Logger log = LoggerFactory.getLogger(VenueDnaAggregator.class);
    public Map<Long, Long> ageIdCounter = new HashMap<Long, Long>();
    public Map<Long, Long> musicIdCounter = new HashMap<Long, Long>();
    public Map<Long, Long> atmosphereIdCounter = new HashMap<Long, Long>();
    public Map<Long, Long> spendingIdCounter = new HashMap<Long, Long>();
    public Long counter = 0l;

    public static Integer MULTIPLIER = 1000;

    public VenueDnaAggregator(VenueActivity activity, Map<Long, UserActivity> userActivity) {
        this.activity = activity;
        userActivityMap = userActivity;
        coreData = CoreData.getInstance();
    }

    public VenueDnaAggregator(VenueActivity activity, Map<Long, UserActivity> userActivity, CoreData coreData) {
        this.activity = activity;
        this.userActivityMap = userActivity;
        this.coreData = coreData;
    }

    private UserActivity getUserActivity(Long userId) {
        UserActivity activity = userActivityMap.get(userId);
        if (activity == null) {
            activity = coreData.getUserActivity(userId);
            userActivityMap.put(userId, activity);
        }
        return activity;
    }

    public CalcVenueDna getCalcVenueDna() {
        if (activity == null) {
            return null;
        }
        clear();

        Integer activitySize = 0;
        for (Long user : activity.upvotes) {
            UserDna dna = coreData.getUserDna(user);
            UserActivity userActivity = getUserActivity(user);
            activitySize = userActivity.upvotes.size();
            if (dna != null) {
                applyToPart(ageIdCounter, dna.ageId, MULTIPLIER / activitySize);
                applyToPart(spendingIdCounter, dna.spendingLimit, MULTIPLIER / activitySize);
                applyToPart(musicIdCounter, dna.musicIds, activitySize);
                applyToPart(atmosphereIdCounter, dna.atmosphereIds, activitySize);
                counter += (MULTIPLIER / activitySize);
            }
        }

        VenueDna upvotedDna = getVenueDna();
        clear();

        for (Long user : activity.downvotes) {
            UserDna dna = coreData.getUserDna(user);
            UserActivity userActivity = getUserActivity(user);
            activitySize = userActivity.downvotes.size();
            if (dna != null) {
                applyToPart(ageIdCounter, dna.ageId, MULTIPLIER / activitySize);
                applyToPart(spendingIdCounter, dna.spendingLimit, MULTIPLIER / activitySize);
                applyToPart(musicIdCounter, dna.musicIds, activitySize);
                applyToPart(atmosphereIdCounter, dna.atmosphereIds, activitySize);
                counter += (MULTIPLIER / activitySize);
            }
        }

        VenueDna downvotedDna = getVenueDna();
        return new CalcVenueDna(upvotedDna, downvotedDna);
    }

    private void clear() {
        ageIdCounter.clear();
        musicIdCounter.clear();
        atmosphereIdCounter.clear();
        spendingIdCounter.clear();
        counter = 0l;
    }

    private VenueDna getVenueDna() {
        VenueDna venueDna = new VenueDna(activity.id);
        venueDna.ageId = determineMax(ageIdCounter);
        venueDna.spendingLimit = determineMax(spendingIdCounter);
        venueDna.atmospherePreferences = determineTop(atmosphereIdCounter, 3);
        venueDna.musicPreferences = determineTop(musicIdCounter, 3);
        venueDna.counter = counter;
        return venueDna;
    }

    private long determineMax(Map<Long, Long> values) {
        Map<Long, Long> sorted = MapUtil.sortByValue(values);
        for (Map.Entry<Long, Long> entry : sorted.entrySet()) {
            return entry.getKey();
        }
        return -1;
    }

    private List<DnaElementCount> determineTop(Map<Long, Long> values, int nValues) {
        List<DnaElementCount> topElements = new ArrayList<DnaElementCount>();
        Map<Long, Long> sorted = MapUtil.sortByValue(values);
        for (Map.Entry<Long, Long> entry : sorted.entrySet()) {
            topElements.add(new DnaElementCount(entry.getKey(), entry.getValue()));
            if (topElements.size() == nValues) {
                break;
            }
        }
        return topElements;
    }

    private static void applyToPart(Map<Long, Long> dnaPiece, List<Long> ids, int activitySize) {
        for (int i = 0; i < ids.size(); i++) {
            Long id = ids.get(i);
            if (id >= 0l) {
                applyToPart(dnaPiece, id, getPosScore(i) / activitySize);
            }
        }
    }

    private static int getPosScore(int pos) {
        if (pos == 0) {
            return MULTIPLIER * 3;
        } else if (pos == 1) {
            return MULTIPLIER * 2;
        } else if (pos == 2) {
            return MULTIPLIER * 1;
        }
        return 0;
    }

    private static void applyToPart(Map<Long, Long> dnaPiece, Long id, int score) {
        Long count = dnaPiece.get(id);
        if (count == null) {
            dnaPiece.put(id, (long) score);
        } else {
            dnaPiece.put(id, count + score);
        }
    }
}
