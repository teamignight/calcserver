package com.ignight.calcs;

import com.ignight.core.CoreData;
import com.ignight.data.TrendingValues;
import com.ignight.data.GroupData;
import com.ignight.data.UserDna;
import com.ignight.databases.RedisKeys;
import com.ignight.exceptions.DomainException;
import com.ignight.messages.QueueMessage;
import com.ignight.messages.QueueMessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

public class GroupCalc extends Calc {
    private static Logger log = LoggerFactory.getLogger(GroupCalc.class);
    public GroupCalc(QueueMessage message) {
        super(message);
    }

    @Override
    public void run() {
        try {
            long begin = System.currentTimeMillis();
            if (message.type == QueueMessageType.RESET) {
                List<Long> allGroups = coreData.getAllGroupIds();
                for (Long groupId : allGroups) {
                    reCalcGroupDna(groupId);
                }
            } else if (message.type == QueueMessageType.LEAVE_GROUP ||
                       message.type == QueueMessageType.JOIN_GROUP ||
                       message.type == QueueMessageType.NEW_PUBLIC_GROUP ||
                       message.type == QueueMessageType.NEW_PRIVATE_GROUP) {
                reCalcGroupDna(message.typeId);
            }
            log.info("calcTime: {} ms", System.currentTimeMillis() - begin);
        } catch (Exception e) {
            log.error("Failed to caclulate Group Calc for " + message, e);
        }
    }

    private void reCalcGroupDna(Long groupId) {
        log.info("Recalcing Group DNA: {}", groupId);
        GroupData g = coreData.getGroup(groupId);
        //TODO: Check if that group exists, has members, has been deleted?
        if (g == null) {
            throw new DomainException("Failed to load Group from cache with id : " + groupId);
        }
        CrowdDnaAggregator aggregator = new CrowdDnaAggregator();
        Iterator<Long> memberIter = g.members.iterator();
        while (memberIter.hasNext()) {
            Long memberId = memberIter.next();
            UserDna dna = coreData.getUserDna(memberId);
            if (dna == null) {
                log.error("DNA Missing For User: {}", memberId);
            } else {
                aggregator.apply(dna);
            }
        }

        TrendingValues groupDna = aggregator.getCollectionDna();
        String key = RedisKeys.getGroupDnaKey(groupId);
        jedis.hset(key, groupId.toString(), groupDna);
    }
}
