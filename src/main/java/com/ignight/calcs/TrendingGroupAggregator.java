package com.ignight.calcs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TrendingGroupAggregator {
    private static final Logger log = LoggerFactory.getLogger(TrendingGroupAggregator.class);
    private Map<Long, TrendingGroup> groupCounter = new HashMap<Long, TrendingGroup>();

    public void apply(Set<Long> userGroups) {
        if (userGroups == null)  {
            return;
        }
        log.debug("Got Groups: " + userGroups);
        for (Long groupId : userGroups) {
            TrendingGroup count = groupCounter.get(groupId);
            if (count == null) {
                log.debug("Creating Trending Group " + groupId);
                count = new TrendingGroup(groupId);
                groupCounter.put(groupId, count);
            }
            count.count += 1l;
            log.debug("Setting Trending Group " + groupId + " count to " + count.count);
        }
    }

    public List<TrendingGroup> determineTopTrending() {
        List<TrendingGroup> groups = new ArrayList<TrendingGroup>(groupCounter.values());
        Collections.sort(groups);
        if (groups.size() <= 3) {
            return groups;
        }
        return groups.subList(0, 3);
    }

    public static class TrendingGroup implements Comparable {
        public Long groupId;
        public Long count;

        public TrendingGroup(Long groupId) {
            this.groupId = groupId;
            this.count = 0l;
        }


        public int compareTo(Object o) {
            TrendingGroup other = (TrendingGroup)o;
            if (count == null && other.count == null) {
                return 0;
            }

            if (count == null) {
                return -1;
            }

            if (other.count == null) {
                return 1;
            }

            return other.count.compareTo(count);
        }
    }
}
