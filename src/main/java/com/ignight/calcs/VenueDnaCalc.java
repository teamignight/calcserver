package com.ignight.calcs;


import com.ignight.data.TrendingValues;
import com.ignight.data.UserDna;
import com.ignight.data.VenueTrending;
import com.ignight.data.VenueActivity;
import com.ignight.databases.RedisKeys;
import com.ignight.messages.QueueMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class VenueDnaCalc extends Calc {
    private static final Logger log = LoggerFactory.getLogger(VenueDnaCalc.class);

    public VenueDnaCalc(QueueMessage message) {
        super(message);
    }

    /*
     * -- Calculate Venue Dna ---
     * 1. Iterate through each up-voted user
     * 2. Add users dna values
     * 3. Get users groups and aggregate
     * 4. Get VenueDna and Trending Groups from Aggregators
     * 5. Save to redis
     */
    @Override
    public void run() {
        try {
            VenueActivity activity = coreData.getVenueActivity(message.typeId);
            if (activity == null) {
                log.error("Activity should never be null since core data was updated right before this");
                return;
            }

            Iterator<Long> upvoteIter = activity.upvotes.iterator();

            CrowdDnaAggregator aggregator = new CrowdDnaAggregator();
            TrendingGroupAggregator groupAggregator = new TrendingGroupAggregator();

            while (upvoteIter.hasNext()) {
                //Get Users Dna
                Long userId = upvoteIter.next();

                UserDna dna = coreData.getUserDna(userId);
                if (dna == null) {
                    log.error("Dna for user : {} is null", userId);
                    continue;
                }

                aggregator.apply(dna);

                //Apply Users Group Values For Trending Groups at Venue
                Set<Long> userGroups = coreData.getUserGroups(userId);
                groupAggregator.apply(userGroups);
            }

            TrendingValues dna = aggregator.getCollectionDna();
            List<TrendingGroupAggregator.TrendingGroup> groups = groupAggregator.determineTopTrending();
            VenueTrending venueTrending = new VenueTrending(dna, groups);

            String key = RedisKeys.getVenueTrendingKey(message.typeId);
            log.info("Setting VenueDna: Key={}, venueId={}, VenueDna: {}", key, message.typeId, gson.toJson(venueTrending));
            jedis.hset(key, message.typeId.toString(), venueTrending);
        } catch (Exception e) {
            log.error("Failed to calculate VenueDna Calc for " + message, e);
        }
    }
}
