package com.ignight.calcs;

import com.ignight.data.UserDna;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class UserToUserDna {
    private static final Logger log = LoggerFactory.getLogger(UserToUserDna.class);
    private static final Double musicWeight = 0.5;
    private static final Double atmosphereWeight = 0.2;
    private static final Double ageWeight = 0.15;
    private static final Double spendingWeight = 0.15;

    private static final Double[][] weights = new Double[3][3];
    private static final double firstChoiceWeight = 0.5;
    private static final double secondChoiceWeight = 0.35;
    private static final double thirdChoiceWeight = 0.15;

    static {
        weights[0][0] = firstChoiceWeight;
        weights[0][1] = secondChoiceWeight;
        weights[0][2] = thirdChoiceWeight;

        weights[1][0] = secondChoiceWeight;
        weights[1][1] = secondChoiceWeight;
        weights[1][2] = thirdChoiceWeight;

        weights[2][0] = thirdChoiceWeight;
        weights[2][1] = thirdChoiceWeight;
        weights[2][2] = thirdChoiceWeight;
    }

    public static double calculateUserToUserDna(final UserDna userOne, final UserDna userTwo) {
        return calculateDnaValue(userOne.clone(), userTwo.clone());
    }

    private static double calculateDnaValue(final UserDna userOne, final UserDna userTwo) {
        double dnaValue = 0;
        if (userOne.validate() && userTwo.validate()) {
            //Spending Limit
            dnaValue += calculateSpendingCompatibility(userOne.spendingLimit, userTwo.spendingLimit) * spendingWeight;
            log.debug("User1 : {}, User2 : {} || After spenidng : {}", userOne.userId, userTwo.userId, dnaValue);
            //Age
            dnaValue += calculateAgeCompatibility(userOne.ageId, userTwo.ageId) * ageWeight;
            log.debug("User1 : {}, User2 : {} || After Age : {}", userOne.userId, userTwo.userId, dnaValue);
            //Music
            dnaValue += calculateCompatibility(userOne.musicIds, userTwo.musicIds) * musicWeight;
            log.debug("User1 : {}, User2 : {} || After Music : {}", userOne.userId, userTwo.userId, dnaValue);
            //Atmosphere
            dnaValue += calculateCompatibility(userOne.atmosphereIds, userTwo.atmosphereIds) * atmosphereWeight;
            log.debug("User1 : {}, User2 : {} || After Atmosphere : {}", userOne.userId, userTwo.userId, dnaValue);
        }
        return dnaValue;
    }

    public static double calculateSpendingCompatibility (long spendingLimitOne, long spendingLimitTwo) {
        long diff = Math.abs(spendingLimitOne - spendingLimitTwo);
        double spendingCompatibility = 0;
        if (diff == 1) {
            spendingCompatibility = 0.5;
        } else if (diff == 0) {
            spendingCompatibility = 1;
        }
        return spendingCompatibility;
    }

    public static double calculateAgeCompatibility(long ageBucketOne, long ageBucketTwo) {
        long diff = Math.abs(ageBucketOne - ageBucketTwo);
        double ageCompatibility = 0;
        if (diff == 1) {
            ageCompatibility = 0.5;
        } else if (diff == 0) {
            ageCompatibility = 1;
        }
        return ageCompatibility;
    }

    public static double calculateCompatibility(List<Long> preferenceOne, List<Long> preferenceTwo) {
        return calculatePreferencesCompatibility(preferenceOne, preferenceTwo);
    }

    private static double calculatePreferencesCompatibility(List<Long> one, List<Long> two) {
        double value = 0;

        fillOutArray(one);
        fillOutArray(two);

        for (int i = 0; i < one.size(); i++) {
            Long currentPreference = one.get(i);
            for (int j = 0; j < two.size(); j++) {
                if (currentPreference.equals(two.get(j))) {
                    value += weights[i][j];
                }
            }
        }
        return Math.min(1.0, value);
    }

    private static void fillOutArray(List<Long> preferences) {
        Iterator<Long> iter = preferences.iterator();
        while (iter.hasNext()) {
            Long val = iter.next();
            if (val < 0) {
                iter.remove();
            }
        }

        long size = preferences.size();

        if (size == 1) {
            Long mainPreference = preferences.get(0);
            preferences.add(mainPreference);
            preferences.add(mainPreference);
        } else if (size == 2) {
            Long mainPreference = preferences.get(0);
            preferences.add(mainPreference);
        }
    }
}
