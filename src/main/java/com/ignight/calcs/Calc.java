package com.ignight.calcs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.core.CoreData;
import com.ignight.data.VenueActivity;
import com.ignight.databases.JedisConnection;
import com.ignight.messages.QueueMessage;
import com.ignight.messages.QueueMessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Calc implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(Calc.class);
    protected CoreData coreData = CoreData.getInstance();

    protected QueueMessage message;
    protected static final JedisConnection jedis = JedisConnection.getInstance();
    protected static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    public Calc(QueueMessage message) {
        this.message = message;
    }
    
    public void run() {
    	
    }
}
