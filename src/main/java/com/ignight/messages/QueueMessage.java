package com.ignight.messages;

public class QueueMessage {
    public Long userId;
    public Long typeId;
    public QueueMessageType type;

    @Override
    public String toString() {
        return "QueueMessage{" +
                "userId=" + userId +
                ", typeId=" + typeId +
                ", type=" + type +
                '}';
    }
}
