package com.ignight.data;

import com.google.gson.annotations.SerializedName;

public class Group {
    @SerializedName("id")
    public Long id;

    @SerializedName("n")
    public String name;

    @SerializedName("c")
    public Long cityId;

    @SerializedName("admin")
    public Long adminId;

    @SerializedName("d")
    public String description;

    @SerializedName("t")
    public boolean isPrivate;
}
