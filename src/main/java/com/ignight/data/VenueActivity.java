package com.ignight.data;

import java.util.HashSet;
import java.util.Set;

public class VenueActivity {
    public Long id;
    public Set<Long> upvotes = new HashSet<Long>();
    public Set<Long> downvotes = new HashSet<Long>();

    public VenueActivity(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "VenueActivity{" +
                "id=" + id +
                ", upvotes=" + upvotes +
                ", downvotes=" + downvotes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return id == ((VenueActivity) o).id;
    }
}
