package com.ignight.data;

import java.util.ArrayList;
import java.util.List;

public class UserActivity {
    public Long userId;
    public List<Long> upvotes = new ArrayList<Long>();
    public List<Long> downvotes = new ArrayList<Long>();

    public UserActivity(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserActivity{" +
                "userId=" + userId +
                ", upvotes=" + upvotes +
                ", downvotes=" + downvotes +
                '}';
    }
}
