package com.ignight.data;

import java.util.HashSet;
import java.util.Set;

public class GroupData {
    public Long id;
    public Long adminId;
    public Boolean isPrivate;
    public Set<Long> members = new HashSet<Long>();

    public GroupData(Group g) {
        this.id = g.id;
        this.adminId = g.adminId;
        this.isPrivate = g.isPrivate;
    }
}
