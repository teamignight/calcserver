package com.ignight.data;

public class TrendingValues {
    public long ageId;
    public long musicId;
    public long atmosphereId;
    public long spendingId;

    public TrendingValues(long age, long music, long atm, long spend) {
        this.ageId = age;
        this.musicId = music;
        this.atmosphereId = atm;
        this.spendingId = spend;
    }
}
