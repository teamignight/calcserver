package com.ignight.data;

import java.util.ArrayList;
import java.util.List;

public class VenueDna {
    public Long venueId;
    public Long counter = 0l;
    public List<DnaElementCount> musicPreferences = new ArrayList<DnaElementCount>();
    public List<DnaElementCount> atmospherePreferences = new ArrayList<DnaElementCount>();
    public long ageId = -1;
    public long spendingLimit = -1;

    public VenueDna(){}

    public VenueDna(long venueId) {
        this.venueId = venueId;
    }

    @Override
    public String toString() {
        return "VenueDna{" +
                "venueId=" + venueId +
                ", counter=" + counter +
                ", musicPreferences=" + musicPreferences +
                ", atmospherePreferences=" + atmospherePreferences +
                ", ageId=" + ageId +
                ", spendingLimit=" + spendingLimit +
                '}';
    }
}
