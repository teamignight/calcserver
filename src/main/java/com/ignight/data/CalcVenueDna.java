package com.ignight.data;

public class CalcVenueDna {
    public Long venueId;
    public VenueDna upvotedDna;
    public VenueDna downvotedDna;

    public CalcVenueDna() {}

    public CalcVenueDna(VenueDna upvotedDna, VenueDna downvotedDna) {
        this.venueId =  upvotedDna.venueId;
        this.upvotedDna = upvotedDna;
        this.downvotedDna = downvotedDna;
    }

    @Override
    public String toString() {
        return "CalcVenueDna{" +
                "venueId=" + venueId +
                ", upvotedDna=" + upvotedDna +
                ", downvotedDna=" + downvotedDna +
                '}';
    }
}
