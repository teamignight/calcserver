package com.ignight.data;

import com.ignight.calcs.TrendingGroupAggregator;

import java.util.ArrayList;
import java.util.List;

public class VenueTrending {
    public TrendingValues trendingValues;
    public List<Long> trendingGroups = new ArrayList<Long>();

    public VenueTrending(TrendingValues dna, List<TrendingGroupAggregator.TrendingGroup> groups) {
        this.trendingValues = dna;
        for (TrendingGroupAggregator.TrendingGroup group : groups) {
            trendingGroups.add(group.groupId);
        }
    }
}
