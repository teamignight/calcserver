package com.ignight.exceptions;

import com.amazonaws.services.cloudsearchv2.model.BaseException;

public class DomainException extends RuntimeException {
    public DomainException() {
    }

    public DomainException(String s) {
        super(s);
    }

    public DomainException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DomainException(Throwable throwable) {
        super(throwable);
    }
}
