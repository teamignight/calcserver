package com.ignight.exceptions;

public class PropertiesException extends RuntimeException {
    public PropertiesException(String message) {
        super(message);
    }

    public PropertiesException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
