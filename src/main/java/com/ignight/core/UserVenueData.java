package com.ignight.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class UserVenueData {
    public Long userId = null;

    public boolean dirty = false;
    public AtomicBoolean lockUpdates = new AtomicBoolean(false);

    public HashMap<Long, Double> userVenueData = new HashMap<Long, Double>();
    public List<Object> locks;

    public UserVenueData(Long userId, int venueSize) {
        this.userId = userId;
        this.locks = new ArrayList<Object>();
        for (int i = 0; i <= venueSize; i++) {
            this.locks.add(new Object());
        }
    }

    public synchronized void lockUpdates() {
        while (!lockUpdates.weakCompareAndSet(false, true));
    }

    public synchronized void unlock() {
        lockUpdates.set(false);
    }

    public void updateDelta(Long venueId, Double delta) {
        while (lockUpdates.get());
        if (venueId == null) {
            return;
        }
        Object lock = locks.get((int) (venueId % CoreData.venueSize));
        synchronized (lock) {
            Double val = userVenueData.get(venueId);
            if (val == null) {
                val = 0.0;
            }
            val += delta;
            userVenueData.put(venueId, val);
            dirty = true;
        }
    }
}
