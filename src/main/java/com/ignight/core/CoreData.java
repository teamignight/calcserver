package com.ignight.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.calcs.UserToUserDna;
import com.ignight.data.Group;
import com.ignight.data.GroupData;
import com.ignight.data.UserActivity;
import com.ignight.data.UserDna;
import com.ignight.data.VenueActivity;
import com.ignight.databases.JedisConnection;
import com.ignight.databases.RedisKeys;
import com.ignight.exceptions.DomainException;
import com.ignight.messages.QueueMessage;
import com.ignight.messages.QueueMessageType;
import com.ignight.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class CoreData {
    private static final Logger log = LoggerFactory.getLogger(CoreData.class);
    protected static final JedisConnection jedis = JedisConnection.getInstance();
    protected static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    public static int venueSize = 0;
    protected static ConcurrentHashMap<Long, UserDna> userDnas = new ConcurrentHashMap<Long, UserDna>();
    private HashSet<Long> activeVenues = new HashSet<Long>();

    private static CoreData INSTANCE  = null;

    protected CoreData() {
    }

    public synchronized static CoreData getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CoreData();
        }
        return INSTANCE;
    }

    public void clear() {
        userDnas.clear();
    }

    /*
     Groups
     */
    public List<Long> getAllGroupIds() {
        List<Long> groups = new ArrayList<Long>();
        String allGroupKey = RedisKeys.GROUP_HASH_KEY_PREFIX + "*";
        Set<String> keys = jedis.keys(allGroupKey);
        for (String key : keys) {
            Set<String> vals = jedis.hkeys(key);
            for (String val : vals) {
                Long group = Long.valueOf(val);
                groups.add(group);
            }
        }
        return groups;
    }

    public GroupData getGroup(Long groupId) {
        String key = RedisKeys.getGroupsBucketKey(groupId);
        String data = jedis.hget(key, groupId.toString());
        if (data != null) {
            Group group = gson.fromJson(data, Group.class);
            GroupData g = new GroupData(group);

            key = RedisKeys.getGroupMembersKey(groupId);
            Set<String> members = jedis.smembers(key);
            Set<Long> result = new HashSet<Long>();
            for (String m : members) {
                g.members.add(Long.valueOf(m));
            }
            return g;
        }
        return null;
    }

    /*
     * User DNA
     */
    public Set<Long> getUserIds() {
        return userDnas.keySet();
    }

    public UserDna getUserDna(Long userId) {
        if (userId == null) {
            return null;
        }
        UserDna dna = userDnas.get(userId);
        if (dna == null) {
            log.info("Loading UserDna : {}", userId);
            return loadUserDna(userId);
        }
        return dna;
    }

    public UserDna loadUserDna(Long userId) {
        String data = jedis.hget(RedisKeys.getUserDnaBucketKey(userId), userId.toString());
        if (data != null) {
            UserDna dna = gson.fromJson(data, UserDna.class);
            //Want to reload it
            UserDna ret = userDnas.put(dna.userId, dna);
            if (ret != null) {
                dna = ret;
            }
            log.info("Updating User: {}, New Dna: {}", userId, dna);
            return dna;
        } else {
            log.error("Failed to load UserDna from cache for user : {}", userId);
            return null;
        }
    }

    public void setUserDna(Long userId, UserDna dna) {
        userDnas.put(userId, dna);
    }

    /*
     * User Groups
     */
    public Set<Long> getUserGroups(Long userId) {
        Set<Long> groups = new HashSet<Long>();
        String key = RedisKeys.getUserGroupsKey(userId);
        Set<String> members = jedis.smembers(key);
        for (String member : members) {
            groups.add(Long.valueOf(member));
        }
        return groups;
    }

    /*
     * Venue Activity
     */

    public synchronized void addVenueActivity(QueueMessage message, UserActivity userActivity) {
        if (message != null) {
            activeVenues.add(message.typeId);
        }
        if (userActivity != null) {
            for (Long a : userActivity.downvotes) {
                activeVenues.add(a);
            }
            for (Long a : userActivity.upvotes) {
                activeVenues.add(a);
            }
        }
        //TODO: Make this more optimal so we only update venues that we need to
//        if (message.typeId != null) {
//            Long activityVenueId = message.typeId;
//            activeVenues.add(activityVenueId);
//            if (userActivity.upvotes.contains(message.typeId)) {
//                if (message.type == QueueMessageType.SWITCH_TO_ADD) { //Was down vote before
//                    for (Long venueId : userActivity.downvotes) {
//                        activeVenues.add(venueId);
//                    }
//                }
//                for (Long venueId : userActivity.upvotes) {
//                    activeVenues.add(venueId);
//                }
//            } else if (userActivity.downvotes.contains(message.typeId)) {
//                if (message.type == QueueMessageType.SWITCH_TO_REMOVE) { //Was upvote before
//                    for (Long venueId : userActivity.upvotes) {
//                        activeVenues.add(venueId);
//                    }
//                }
//                for (Long venueId : userActivity.downvotes) {
//                    activeVenues.add(venueId);
//                }
//            } else {
//                if (message.type == QueueMessageType.ADD_ACTIVITY) {
//                    for (Long venueId : userActivity.upvotes) {
//                        activeVenues.add(venueId);
//                    }
//                } else if (message.type == QueueMessageType.REMOVE_ACTIVITY) {
//                    for (Long venueId : userActivity.downvotes) {
//                        activeVenues.add(venueId);
//                    }
//                }
//            }
//        }
    }

    public synchronized HashSet<Long> getAndClearActiveVenues() {
        HashSet<Long> copySet = new HashSet<Long>();
        for (Long venue : activeVenues) {
            copySet.add(venue);
        }
        activeVenues.clear();
        return copySet;
    }

//    public Set<Long> getActiveVenues() {
//        Set<Long> venues = new HashSet<Long>();
//        String key = RedisKeys.VENUE_ACTIVITY_KEY_PREFIX + "*";
//        Set<String> allKeys = jedis.keys(key);
//        for (String venueKey : allKeys) {
//            String venueId = venueKey.split("::")[1];
//            venues.add(Long.valueOf(venueId));
//        }
//        return venues;
//    }

    public VenueActivity getVenueActivity(Long venueId) {
        VenueActivity venueActivity = new VenueActivity(venueId);
        String key = RedisKeys.getVenueActivityKey(venueId);
        Set<String> activities = jedis.smembers(key);
        for (String activity : activities) {
            Long val = Long.valueOf(activity);
            if (val < 0) {
                venueActivity.downvotes.add(Math.abs(val));
            } else {
                venueActivity.upvotes.add(val);
            }
        }
        return venueActivity;
    }

    public UserActivity getUserActivity(Long userId) {
        UserActivity userActivity = new UserActivity(userId);
        String key = RedisKeys.getUserActivityKey(userId);
        Set<String> activity = jedis.smembers(key);

        if (activity != null) {
            for (String s : activity) {
                Long val = Long.valueOf(s);
                if (val < 0) {
                    userActivity.downvotes.add(-1 * val);
                } else {
                    userActivity.upvotes.add(val);
                }
            }
        }
        return userActivity;
    }
}
