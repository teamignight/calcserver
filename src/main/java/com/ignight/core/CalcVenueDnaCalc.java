package com.ignight.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.calcs.VenueDnaAggregator;
import com.ignight.data.CalcVenueDna;
import com.ignight.data.UserActivity;
import com.ignight.data.VenueActivity;
import com.ignight.databases.JedisConnection;
import com.ignight.databases.RedisKeys;
import com.ignight.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CalcVenueDnaCalc implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(CalcVenueDnaCalc.class);
    private static final JedisConnection jedis = JedisConnection.getInstance();
    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    private static CoreData coreData = CoreData.getInstance();
    private ConcurrentHashMap<Long, UserActivity> userActivityMap = new ConcurrentHashMap<Long, UserActivity>();
    private Map<String, Map<String, String>> venueDnaBucketsToValues = new HashMap<String, Map<String, String>>();

    private Timer timer = new Timer();
    private static String CALC_VENUE_DNA = "Calculating Venue Dna";
    private static String UPDATING_DB = "Updating Venue Dna In DB";

    public CalcVenueDnaCalc() {
        logger.info("--- Updating Calc Venue Dna --");
    }

    @Override
    public void run() {
        try {
            timer.beginTime(CALC_VENUE_DNA);
            Map<Long, UserActivity> userActivityMap = new HashMap<Long, UserActivity>();

            //Gather All Venue Dna Values
            HashSet<Long> activeVenues = coreData.getAndClearActiveVenues();
            for (Long venueId : activeVenues) {
                try {
                    VenueActivity venueActivity = coreData.getVenueActivity(venueId);
                    VenueDnaAggregator dnaAggregator = new VenueDnaAggregator(venueActivity, userActivityMap);
                    CalcVenueDna calcVenueDna = dnaAggregator.getCalcVenueDna();
                    addCalcVenueDna(calcVenueDna);
                } catch (Exception e) {
                    logger.error("Failed to calculate CalcVenueDna for venue: {}", venueId, e);
                }
            }
            timer.endTime(CALC_VENUE_DNA);
            timer.beginTime(UPDATING_DB);
            //Update the Dna Values
            for (Map.Entry<String, Map<String, String>> entry : venueDnaBucketsToValues.entrySet()) {
                String key = entry.getKey();
                Map<String, String> venueValues = entry.getValue();
                if (venueValues.size() > 0) {
                    jedis.hmset(key, venueValues);
                }
            }
            timer.endTime(UPDATING_DB);
        } catch (Exception e) {
            logger.error("Failed to Update Venue Dna Values", e);
        } finally {
            logger.info("--- Venue Calc Dna CalcTime: {}", timer.print());
            timer.clear();
            userActivityMap.clear();
            venueDnaBucketsToValues.clear();
        }
    }

    private void addCalcVenueDna(com.ignight.data.CalcVenueDna venueDna) {
        String key = RedisKeys.getVenueDnaKey();
//        String key = RedisKeys.getVenueDnaKey(venueDna.venueId);
        Map<String, String> venueValues = venueDnaBucketsToValues.get(key);
        if (venueValues == null) {
            venueValues = new HashMap<String, String>();
            venueDnaBucketsToValues.put(key, venueValues);
        }
        venueValues.put(venueDna.venueId.toString(), gson.toJson(venueDna));
    }
}
