package com.ignight.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalcProperties {
    protected static Logger log = LoggerFactory.getLogger(CalcProperties.class);

    private static String redisHost;
    private static String sqsQueue;
    private static Long venueDnaThreadInitialDelayMillis;
    private static Long venueDnaThreadPeriodMillis;

    static {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("/properties/calc.properties");
            prop.load(input);

            redisHost = prop.getProperty("redisHost");
            sqsQueue = prop.getProperty("sqsQueue");
            venueDnaThreadInitialDelayMillis = Long.valueOf(prop.getProperty("venueDnaThreadInitialDelayMillis"));
            venueDnaThreadPeriodMillis = Long.valueOf(prop.getProperty("venueDnaThreadPeriodMillis"));

        } catch (IOException ex) {
            log.error("Failed to load properties file", ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.error("Failed to close properties file", e);
                }
            }
        }
    }

    public static String getRedisHost() {
        return redisHost;
    }

    public static String getSqsQueue() {
        return sqsQueue;
    }

    public static Long getVenueDnaThreadInitialDelayMillis() {
        return venueDnaThreadInitialDelayMillis;
    }

    public static Long getVenueDnaThreadPeriodMillis() {
        return venueDnaThreadPeriodMillis;
    }
}
