package com.ignight.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Timer {
    public Map<String, KeyValues> times = new HashMap<String, KeyValues>();
    public Map<String, Long> current = new HashMap<String, Long>();
    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    public void clear() {
        times.clear();
        current.clear();
    }

    public void beginTime(String key) {
        current.put(key, System.currentTimeMillis());
    }

    public void endTime(String key) {
        Long beg = current.get(key);
        if (beg != null) {
            long total = System.currentTimeMillis() - beg;
            add(key, total);
        } else {
            current.remove(key);
        }
    }

    private synchronized void add(String key, Long total) {
        KeyValues val = times.get(key);
//        Long val = times.get(key);
        if (val == null) {
            val = new KeyValues();
        }
        val.time += total;
        val.count++;
        times.put(key, val);
    }

    public static class KeyValues {
        public Long time = 0l;
        public Long count = 0l;
    }

    public String print() {
        Map<String, Object> json = new HashMap<String, Object>();
        for (Map.Entry<String, KeyValues> entry : times.entrySet()) {
            KeyValues val = entry.getValue();
            ArrayList<Object> arr = new ArrayList<Object>();
            arr.add(val.count);
            arr.add(val.time + " ms");
            json.put(entry.getKey(), arr);
        }
        return gson.toJson(json);
    }
}
