package com.ignight.loaders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.core.CoreData;
import com.ignight.data.UserDna;
import com.ignight.databases.JedisConnection;
import com.ignight.databases.RedisKeys;
import com.ignight.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

public class UserDataLoader implements Callable<Void> {
    private static Logger log = LoggerFactory.getLogger(VenueDataLoader.class);
    private CoreData coreData = CoreData.getInstance();
    private static Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public static final JedisConnection jedis = JedisConnection.getInstance();
    public Timer timer = new Timer();

    private void loadUserDnas() {
        String buckets  = RedisKeys.getUserDnaBucketPrefix() + "*";
        Set<String> keys = jedis.keys(buckets);
        for (String bucket : keys) {
            Map<String, String> userDnas = jedis.hgetall(bucket);
            for (Map.Entry<String, String> entry : userDnas.entrySet()) {
                Long userId = Long.valueOf(entry.getKey());
                UserDna dna = gson.fromJson(entry.getValue(), UserDna.class);
                coreData.setUserDna(userId, dna);
            }
        }
    }

    @Override
    public Void call() throws Exception {
        try {
            loadUserDnas();
        } catch (Exception e) {
            log.error("Failed to load user dna", e);
        }
        return null;
    }
}
