package com.ignight.loaders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ignight.data.VenueActivity;
import com.ignight.databases.JedisConnection;
import com.ignight.databases.RedisKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class VenueDataLoader implements Runnable {
    private static Logger log = LoggerFactory.getLogger(VenueDataLoader.class);
    private static Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public static final JedisConnection jedis = JedisConnection.getInstance();

    public static void loadVenueActivity() {
        String allKeys = RedisKeys.getVenueActivityPrefix() + "*";
        Set<String> activityKeys = jedis.keys(allKeys);
        for (String key : activityKeys) {
            String[] splitArr = key.split("::");
            if (splitArr.length == 2) {
                Long venueId = Long.valueOf(splitArr[1]);
                Set<String> userActivity = jedis.smembers(key);
                if (userActivity != null && userActivity.size() > 0) {
                    VenueActivity venueActivity = new VenueActivity(venueId);
                    for (String activity : userActivity) {
                        Long val = Long.valueOf(activity);
                        if (val < 0) {
                            venueActivity.downvotes.add(Math.abs(val));
                        } else {
                            venueActivity.upvotes.add(Math.abs(val));
                        }
                    }
//                    CoreData.setVenueActivity(venueId, venueActivity);
                }
            } else {
                log.error("Found venue activity key not set correctly: {}", key);
            }
        }
    }

    public void run() {
        try {
            loadVenueActivity();
        } catch (Exception e) {
            log.error("Failed to load venue activities", e);
        }
    }
}