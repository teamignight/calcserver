#clean build
mvn clean

#create JAR
mvn package

#upload if exists
if [ -f 'target/calcserver-1.0-SNAPSHOT.jar' ]; then
    echo "Uploading JAR to PROD..."
    aws s3 cp target/calcserver-1.0-SNAPSHOT.jar s3://prod.resources.ignight/artifacts/
else
    echo "JAR file missing in target/"
fi
